function saveLocalStorage() {
    localStorage.setItem("footer-name", $("#footer-name").val());
    localStorage.setItem("footer-number", $("#footer-number").val());
    localStorage.setItem("footer-email", $("#footer-email").val());
    localStorage.setItem("footer-message", $("#footer-message").val());
    localStorage.setItem("footer-policy", $("#footer-policy").prop("checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("footer-name") !== null)
        $("#footer-name").val(localStorage.getItem("footer-name"));
    if (localStorage.getItem("footer-number") !== null)
        $("#footer-number").val(localStorage.getItem("footer-number"));
    if (localStorage.getItem("footer-email") !== null)
        $("#footer-email").val(localStorage.getItem("footer-email"));
    if (localStorage.getItem("footer-message") !== null)
        $("#footer-message").val(localStorage.getItem("footer-message"));
    if (localStorage.getItem("footer-policy") !== null) {
        $("#footer-policy").prop("checked", localStorage.getItem("footer-policy") === "true");
        if ($("#footer-policy").prop("checked"))
            $("#sendButton").removeAttr("disabled");
    }
}
function clear() {
    localStorage.clear()
    $("#footer-name").val("");
    $("#footer-number").val("");
    $("#footer-email").val("");
    $("#footer-message").val("");
    $("#footer-policy").val(false);
}

$(document).ready(function() {
    loadLocalStorage();
    $("#footer-form").submit(function(e) {
        e.preventDefault();
        let data =  $(this).serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/WGx38iF8c5",
            data: data,
            success: function(response){
                if(response.status == "success"){
                    alert("Спасибо за сообщение!");
                    clear();
                } else {
                    alert("Произошла ошибка: " + response.message);
                }
            }
        });
    });
    $("#footer-policy").change(function() {
        if((!this.checked)&&(grecaptcha.getResponse() === ""))
        $("#sendButton").attr("disabled", "");    
        else
        $("#sendButton").removeAttr("disabled");
    })
    $("#form").change(saveLocalStorage);
})
